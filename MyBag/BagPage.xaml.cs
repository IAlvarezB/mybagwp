﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Xml.Linq;

namespace MyBag
{
    public partial class BagPage : PhoneApplicationPage
    {
        const string FORMAT_DATE = "yyyy-MM-dd";
       

        String destinationPlace;
        DateTime departureDate;
        DateTime returnDate;

        public BagPage()
        {
            InitializeComponent();

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            destinationPlace = (String)PhoneApplicationService.Current.State["destinationPlace"];
            departureDate = (DateTime)PhoneApplicationService.Current.State["departureDate"];
            returnDate = (DateTime)PhoneApplicationService.Current.State["returnDate"];
            getWeatherAPI(destinationPlace, departureDate, returnDate);

        }

        private void volverBtn_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            
        }

        private void getWeatherAPI(String destinationPlace, DateTime departureDate, DateTime returnDate)
        {
            //0efac0805abc39b7d84ae15ee5f03643

            WebClient client = new WebClient();
            client.DownloadStringAsync(new Uri("http://api.openweathermap.org/data/2.5/forecast/daily?q="+destinationPlace+"&mode=xml&units=metric&cnt=16&appid=0efac0805abc39b7d84ae15ee5f03643"));
            client.DownloadStringCompleted += parseoXml;

        }

        private void parseoXml(object sender, DownloadStringCompletedEventArgs e)
        {
            int countDays = 0;

            int sleevedLongDay = 0;
            int tShirtDay = 0;
            int sweatTshirt = 0;
            int coatDay = 0;
            Boolean umbrella = false;
            int pantsDay = 0;
            int shortsDay = 0;
            int sneakersDay = 0;
            int bootsDay = 0;

            if (e.Error == null)
            {
                XDocument xdoc = XDocument.Parse(e.Result, LoadOptions.None);

                IEnumerable<XElement> times = xdoc.Descendants(XName.Get("time"));
                foreach (XElement time in times)
                {
                    String day = time.Attribute("day").Value;
                    DateTime dtDay = DateTime.ParseExact(day, FORMAT_DATE, System.Globalization.CultureInfo.InvariantCulture);
                    
                    //Empezamos a parsear datos cuando el día sea el mismo que el de salida
                    if(dtDay.CompareTo(departureDate) >= 0 && dtDay.CompareTo(returnDate) <= 0) {

                        countDays++;
                        XElement temperature = time.Element(XName.Get("temperature"));

                        float tempMedia = float.Parse(temperature.Attribute("day").Value);
                        if (tempMedia >= 20.0)
                        {
                            //Tiempo de camiseta de manga corta y pantalones cortos
                            tShirtDay++;
                            shortsDay++;
                        }
                        else if (tempMedia < 20.0 && tempMedia >= 15.0)
                        {
                            //Tiempo de camiseta de manda larga y pantalones largos
                            sleevedLongDay++;
                            pantsDay++;
                        }
                        else if (tempMedia < 15.0 && tempMedia >= 10.0)
                        {
                            //Tiempo de sudadera
                            sweatTshirt ++;
                            tShirtDay++;
                            pantsDay++;
                        }
                        else if (tempMedia < 10.0)
                        {
                            //Tiempo de abrigo
                            coatDay++;
                            sweatTshirt++;
                            pantsDay++;

                            if (tempMedia > 0.0)
                            {
                                tShirtDay++;
                            }
                            else
                            {
                                sleevedLongDay++;
                            }
                        }

                        
                        //Si llueve nos llevamos el paraguas
                        XElement precipitation = time.Element(XName.Get("precipitation"));
                        if (precipitation.HasAttributes)
                        {
                                bootsDay++;
                                umbrella = true;
                        }
                        else
                        {
                            sneakersDay++;
                        }

                    }else if(dtDay.CompareTo(returnDate) > 0)
                    {
                        //Una vez el día haya sobrepasado al día de retorno, no hace falta analizar mas.
                        break;
                    }
                }

                //Metodo que calcula y muestra todo en pantalla
                makeMeTheBag(sleevedLongDay,tShirtDay,sweatTshirt, coatDay, shortsDay, pantsDay, sneakersDay, bootsDay, umbrella);
            }
        }

        private void makeMeTheBag(int sleevedLong, int tShirt, int sweatTshirt, int coat, int shorts, int pants, int sneakers, int boots, Boolean umbrella)
        {
            numMangaLarga.Text = "x" + sleevedLong;
            numMangaCorta.Text = "x" + tShirt;
            numSudadera.Text = "x" + sweatTshirt;
            numPantalonesCortos.Text = "x" + shorts;
            numPantalonesLargos.Text = "x" + pants;

            if (umbrella)
            {
                paraguas.Text = "SI";
            }
            else
            {
                paraguas.Text = "NO";
            }

            double coatsTotally = Math.Ceiling(coat / 6D);
            numAbrigo.Text = "x" + coatsTotally.ToString();

            double sneakersTotally = Math.Ceiling(sneakers / 6D);
            numZapatillas.Text = "x" + sneakersTotally.ToString();

            double bootsTotally = Math.Ceiling(boots / 6D);
            numBotas.Text = "x" + bootsTotally.ToString();
        }
    }
}