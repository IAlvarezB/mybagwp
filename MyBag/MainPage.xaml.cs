﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyBag.Resources;
using System.IO;


namespace MyBag
{
  
    public partial class MainPage : PhoneApplicationPage
    {
        const string FORMAT_DATE = "dd/MM/yy";
        const int DAYS_LIMIT = 16;
        const int NUM_CERO = 0;


        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            errorTxt.Text = "";
            String departure = departureDateTxt.Text;
            String returnString = returnDateTxt.Text;

            if (formatDate(departure) && formatDate(returnString) && !destinationPlaceTxt.Text.Equals(""))
            {
                DateTime dtDeparture = DateTime.ParseExact(departure, FORMAT_DATE, System.Globalization.CultureInfo.InvariantCulture);
                DateTime dtReturn = DateTime.ParseExact(returnString, FORMAT_DATE, System.Globalization.CultureInfo.InvariantCulture);
                if (controlDays(dtReturn, dtDeparture))
                {
                    PhoneApplicationService.Current.State["destinationPlace"] = destinationPlaceTxt.Text;
                    PhoneApplicationService.Current.State["departureDate"] = dtDeparture;
                    PhoneApplicationService.Current.State["returnDate"] = dtReturn;
                    NavigationService.Navigate(new Uri("/BagPage.xaml", UriKind.Relative));                    
                }

            }

            if (destinationPlaceTxt.Text.Equals(""))
            {
                errorTxt.Text = "Enter destination name.";
            }
        }

        private Boolean formatDate(String date)
        {
            char[] dateChar = new char[date.Length];
            StringReader sr = new StringReader(date);
            sr.Read(dateChar, 0, date.Length);
            if (date.Length != 8)
            {
                errorTxt.Text = "Incorrect length of the entered date.";
                return false;
            }
            else
            {

                for (int i = 0; i < dateChar.Length; i++)
                {
                    if (dateChar[i].Equals("") ||
                        ((i == 2 || i == 5) && !dateChar[i].Equals('/')))
                    {
                        errorTxt.Text = "Wrong date format.";
                        return false;
                    }
                }
            }
            return true;
        }
        private Boolean controlDays(DateTime dateReturn, DateTime dateDeparture)
        {
            DateTime tiempoActual = DateTime.Now;

            if (dateReturn.CompareTo(dateDeparture) < NUM_CERO)
            {
                //El dia de vuelta del viaje es anterior al dia de salida.
                errorTxt.Text = "Departure date should be before that return date.";
                return false;
            }
            else if(dateDeparture.CompareTo(tiempoActual) < NUM_CERO)
            {
                //El día de la salida es anterior al presente dia.
                errorTxt.Text = "Departure date should be after that today.";
                return false;
            }
            else {

                TimeSpan ts = dateReturn - tiempoActual;

                // Comprobamos que la diferencia de dias no es superior a 16.
                int differenceInDays = ts.Days;
                if (ts.Days > DAYS_LIMIT)
                {
                    errorTxt.Text = "16 day forecast limit exceeded.";
                    return false;
                }

            }
            return true;
        }
     
    }
}